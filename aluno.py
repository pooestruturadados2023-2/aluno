class Aluno:
    def __init__(self, matricula:int, nome:str, notas:list):
        self.__matricula=matricula
        self.__nome=nome
        self.__notas=notas
    
    @property
    def get_nome(self):
        return self.__nome
    
    @property
    def get_matricula(self):
        return f'{"%08d" % self.__matricula}'
    
    def set_nome(self,nome):
        self.__nome = nome
    
    def media(self):
        media=0
        for i in self.__notas:
            media+=i
        return round((media/len(self.__notas)),1)
    
    def adiciona_nota(self,nota):
        if isinstance(nota,int) is True or isinstance(nota,float) is True: #checa se o valor passado é um inteiro ou float
            if nota > 10:
                self.__notas.append(10)
            elif nota < 0:
                self.__notas.append(0)
            else:
                self.__notas.append(nota)
        else:
            print('Nota inválida!')

if __name__ == '__main__':
    #criando os alunos
    a1=Aluno(1236,'Joao',[7.9,6.5])
    a2=Aluno(84315,'Maria',[10.0,9.7])

    #printando suas informações
    print('matricula:', a1.get_matricula,'aluno:', a1.get_nome,'media:', a1.media())  
    print('matricula:', a2.get_matricula,'aluno:', a2.get_nome,'media:', a2.media())

    #testando método de adicionar nota
    a1.adiciona_nota(9.8)
    a2.adiciona_nota(7)
    print(f'media de {a1.get_nome}: {a1.media()}')
    print(f'media de {a2.get_nome}: {a2.media()}')
    
    #testando tratamento de erro para caso a nota não seja um número
    a1.adiciona_nota('9.8')
    print(f'media de {a1.get_nome}: {a1.media()}')